package utils;

import com.sun.prism.sw.SWMaskTexture;
import datastorage.*;
import model.*;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * singleton class handles the delete of old data
 */
public class DeleteHandler {
    private static DeleteHandler instance = null;
    private DeleteHandler(){}

    public static DeleteHandler getInstance(){
        if(instance == null){
            instance = new DeleteHandler();
        }
        return instance;
    }
    /**
     * this methods actually deletes the data if its older than 10 years
     */
    public void deleteTenYearOlds(){
        PatientDAO pDAO = DAOFactory.getDAOFactory().createPatientDAO();
        LockedPatientDAO lpDAO = DAOFactory.getDAOFactory().createLockedPatientDAO();
        TreatmentDAO tDAO = DAOFactory.getDAOFactory().createTreatmentDAO();
        LockedTreatmentDAO ltDAO = DAOFactory.getDAOFactory().createLockedTreatmentDAO();
        CaregiverDAO cDAO = DAOFactory.getDAOFactory().createCaregiverDAO();
        LockedCaregiverDAO lcDAO = DAOFactory.getDAOFactory().creatLockedCaregiverDAO();

        ArrayList<Long> cToDelete = checkForCertainDeleteTime("caregiver");
        for(long cKey:cToDelete){
            try{
                cDAO.deleteById(cKey);
            } catch(SQLException e){
                e.printStackTrace();
            }
        }

        ArrayList<Long> lcToDelete = checkForCertainDeleteTime("lockedcaregiver");
        for(long lcKey:lcToDelete){
            try{
                lcDAO.deleteById(lcKey);
            } catch(SQLException e){
                e.printStackTrace();
            }
        }

        ArrayList<Long> pToDelete = checkForCertainDeleteTime("patient");
        for(long pKey:pToDelete){
            try{
                tDAO.deleteByPid(pKey);
                pDAO.deleteById(pKey);
            } catch(SQLException e){
                e.printStackTrace();
            }
        }

        ArrayList<Long> lpToDelete = checkForCertainDeleteTime("lockedPatient");
        for(long lpKey:lpToDelete){
            try{
                ltDAO.deleteByPid(lpKey);
                lpDAO.deleteById(lpKey);
            } catch(SQLException e){
                e.printStackTrace();
            }
        }

    }
    /**
     * this methods collects all key to delete regarding a specific keyword
     *
     * @param keyWord the keyword
     */
    private ArrayList<Long> checkForCertainDeleteTime(String keyWord) {
        PatientDAO ppDAO = DAOFactory.getDAOFactory().createPatientDAO();
        LockedPatientDAO lpDAO = DAOFactory.getDAOFactory().createLockedPatientDAO();
        LockedTreatmentDAO ltDAO = DAOFactory.getDAOFactory().createLockedTreatmentDAO();
        TreatmentDAO tDAO = DAOFactory.getDAOFactory().createTreatmentDAO();
        CaregiverDAO cDAO = DAOFactory.getDAOFactory().createCaregiverDAO();
        LockedCaregiverDAO lcDAO = DAOFactory.getDAOFactory().creatLockedCaregiverDAO();
        ArrayList<Long> result = new ArrayList<>();

        try{
            switch(keyWord){
                case "patient":
                    for(Patient p:ppDAO.readAll()){
                        long key = p.getPid();
                        for (Treatment t:tDAO.readTreatmentsByPid(key)){
                            p.add(t);
                        }
                        if(p.getOldestTreatment()+10<LocalDate.now().getYear()){
                            result.add(key);
                        }
                    }
                    break;
                case "lockedPatient":
                    for(LockedPatient lp:lpDAO.readAll()){
                        long key = lp.getPid();
                        for (LockedTreatment lt:ltDAO.readTreatmentsByPid(key)){
                            lp.add(lt);
                        }
                        if(lp.getOldestTreatment()+10<LocalDate.now().getYear()){
                            result.add(key);
                        }
                    }
                    break;
                case "caregiver":
                    for(Caregiver c:cDAO.readAll()){
                        long key = c.getCaregiverID();
                        if(c.getNewestTreatment()+10<LocalDate.now().getYear()){
                            result.add(key);
                        }
                    }
                    break;
                case "lockedcaregiver":
                    for(LockedCaregiver lc:lcDAO.readAll()){
                        long key = lc.getCaregiverID();
                        if(lc.getNewestTreatment()+10<LocalDate.now().getYear()){
                            result.add(key);
                        }
                    }
                    break;

            }

        } catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }


}
