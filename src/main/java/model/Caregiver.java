package model;

import datastorage.DAOFactory;
import datastorage.LockedTreatmentDAO;
import datastorage.TreatmentDAO;
/**
 * Caregiver Objects contain all business information for caregivers of the nursing home
 */
public class Caregiver extends Person {
    private long caregiverID;
    private String telephoneNumber;

    public Caregiver(long caregiverID, String firstname, String surname, String telephoneNumber) {
        super(firstname, surname);
        this.caregiverID = caregiverID;
        this.telephoneNumber = telephoneNumber;
    }

    public Caregiver(String firstname, String surname, String telephoneNumber) {
        super(firstname, surname);
        this.telephoneNumber = telephoneNumber;
    }

    public long getCaregiverID() {
        return caregiverID;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }
    public void setTelephoneNumber(String TelephoneNumber) {
        this.telephoneNumber = TelephoneNumber;
    }

    public String toString() {
        return "Caregiver" + "\nID: " + this.caregiverID +
                "\nFirstname: " + this.getFirstName() +
                "\nSurname: " + this.getSurname() +
                "\nTelephone Number: " + this.getTelephoneNumber() +
                "\n";
    }

    public int getNewestTreatment(){
        int oldestTreatment = 0;
        LockedTreatmentDAO ltDAO = DAOFactory.getDAOFactory().createLockedTreatmentDAO();
        TreatmentDAO tDAO = DAOFactory.getDAOFactory().createTreatmentDAO();
        try {
            for (LockedTreatment lt : ltDAO.getReadAllTreatmentsOfOneCaregiverByCaregiverid(getCaregiverID())) {
                int toCompare = Integer.parseInt(lt.getDate().substring(0, 4));
                if (oldestTreatment < toCompare) {
                    oldestTreatment = toCompare;
                }
            }
        } catch(NullPointerException e){
            oldestTreatment = oldestTreatment;
        }
        try{
            for(Treatment t:tDAO.getReadAllTreatmentsOfOneCaregiverByCaregiverid(getCaregiverID())){
                int toCompare =Integer.parseInt(t.getDate().substring(0,4));
                if(oldestTreatment<toCompare){
                    oldestTreatment = toCompare;
                }
            }
        } catch(NullPointerException e){
            oldestTreatment = 9999;
        }

        return oldestTreatment;
    }
}

