package model;

public class User {
    private String name;
    private String passwort;

    public User(String name, String passwort) {
        this.name = name;
        this.passwort = passwort;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }

    public String getName() {
        return name.toString();
    }

    public String getPasswort() {
        return passwort.toString();
    }

    public String toString() {
        return "Zugansdaten" +
                "\nBenutzer: " + this.getName() +
                "\nPasswort: " + this.getPasswort() +
                "\n";
    }

}