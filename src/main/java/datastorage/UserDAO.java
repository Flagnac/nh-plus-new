package datastorage;

import model.User;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class UserDAO extends DAOimp<User> {

    public UserDAO(Connection conn) {
        super(conn);
    }

    @Override
    protected String getCreateStatementString(User user) {
        return null;
    }

    @Override
    protected String getReadByIDStatementString(long key) {
        return null;
    }

    @Override
    protected User getInstanceFromResultSet(ResultSet set) throws SQLException {
        User user = new User(set.getString("name"), set.getString("passwort"));
        return user;
    }

    @Override
    protected String getReadAllStatementString() {
        return null;
    }

    @Override
    protected ArrayList<User> getListFromResultSet(ResultSet set) throws SQLException {
        return null;
    }

    @Override
    protected String getUpdateStatementString(User user) {
        return null;
    }

    @Override
    protected String getDeleteStatementString(long key) {
        return null;
    }

    /**
     * generates a <code>SELECT * FROM</code>-Statement to find a user
     * @param user for whom the matching password is picked out
     * @return <code>User</code> with the corresponding password.
     * @throws SQLException
     */

    public User getUserlogindata(String user) throws SQLException {
        User object = null;
        Statement st = conn.createStatement();
        ResultSet result = st.executeQuery(String.format("SELECT * FROM USER WHERE username = '%s'", user));

        if (!result.next()) {
            return null;
        }
        else {
            object = new User(result.getString("username"), result.getString("password"));
            return object;
        }
    }
}
