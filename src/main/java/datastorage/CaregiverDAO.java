package datastorage;

import model.Caregiver;
import model.Patient;
import utils.DateConverter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
/**
 * Implements the Interface <code>DAOImp</code>. Overrides methods to generate specific treatment-SQL-queries.
 */
public class CaregiverDAO extends DAOimp<Caregiver> {
    /**
     * constructs Object. Calls the Constructor from <code>DAOImp</code> to store the connection.
     *
     * @param conn SQL Connection
     */
    public CaregiverDAO(Connection conn) {
        super(conn);
    }
    /**
     * generates a <code>INSERT INTO</code>-Statement for a given patient
     *
     * @param caregiver for which a specific INSERT INTO is to be created
     * @return <code>String</code> with the generated SQL.
     */
    protected String getCreateStatementString(Caregiver caregiver) {
        return String.format("INSERT INTO CAREGIVER (surname, firstname, telephonenumber) VALUES ('%s', '%s', '%s')",
                caregiver.getSurname(), caregiver.getFirstName(), caregiver.getTelephoneNumber());
    }
    /**
     * generates a <code>select</code>-Statement for a given key
     *
     * @param key for which a specific SELECTis to be created
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getReadByIDStatementString(long key) {
        return String.format("SELECT * FROM caregiver WHERE CAREGIVERID = %d", key);

    }
    /**
     * maps a <code>ResultSet</code> to a <code>Patient</code>
     *
     * @param result ResultSet with a single row. Columns will be mapped to a patient-object.
     * @return patient with the data from the resultSet.
     */
    @Override
    protected Caregiver getInstanceFromResultSet(ResultSet result) throws SQLException {
        Caregiver c = null;
        c = new Caregiver(result.getInt(1), result.getString(2), result.getString(3),
                result.getString(4));
        return c;
    }
    /**
     * generates a <code>SELECT</code>-Statement for all caregivers.
     *
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getReadAllStatementString() {
        return "SELECT * FROM CAREGIVER";
    }
    /**
     * maps a <code>ResultSet</code> to a <code>Caregiver-List</code>
     *
     * @param result ResultSet with a multiple rows. Data will be mapped to caregiver-object.
     * @return ArrayList with caregivers from the resultSet.
     */
    @Override
    protected ArrayList<Caregiver> getListFromResultSet(ResultSet result) throws SQLException {
        ArrayList<Caregiver> caregivers = new ArrayList<Caregiver>();
        Caregiver caregiver = null;
        while (result.next()) {
            caregiver = new Caregiver(result.getInt(1), result.getString(2), result.getString(3), result.getString(4));
            caregivers.add(caregiver);
        }
        return caregivers;
    }
    /**
     * generates a <code>UPDATE</code>-Statement for a given caregiver
     *
     * @param caregiver for which a specific update is to be created
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getUpdateStatementString(Caregiver caregiver) {
        return String.format("UPDATE caregiver SET" +
                        "firstname = '%s'," +
                        "surname = '%s'," +
                        "telephoneNumber = '%s'" +
                        "WHERE caregiverid = %d",
                caregiver.getFirstName(),
                caregiver.getSurname(),
                caregiver.getTelephoneNumber(),
                caregiver.getCaregiverID());
    }

    /**
     * generates a <code>delete</code>-Statement for a given key
     *
     * @param key for which a specific DELETE is to be created
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getDeleteStatementString(long key) {
        return String.format("Delete FROM caregiver WHERE caregiverid=%d", key);
    }
}


