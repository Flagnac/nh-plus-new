package datastorage;

import model.Caregiver;
import model.LockedCaregiver;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
/**
 * äquivalent to link{@CaregiverDAO} for locking purpose
 */
public class LockedCaregiverDAO extends DAOimp<LockedCaregiver> {

    public LockedCaregiverDAO(Connection conn) {
        super(conn);
    }

    protected String getCreateStatementString(LockedCaregiver lCaregiver) {
        return String.format("INSERT INTO LOCKEDCAREGIVER (caregiverid, surname, firstname, telephonenumber) VALUES ('%d','%s', '%s', '%s')",
                lCaregiver.getCaregiverID() ,lCaregiver.getSurname(), lCaregiver.getFirstName(), lCaregiver.getTelephoneNumber());
    }

    @Override
    protected String getReadByIDStatementString(long key) {
        return String.format("SELECT * FROM lockedcaregiver WHERE CAREGIVERID = %d", key);

    }

    @Override
    protected LockedCaregiver getInstanceFromResultSet(ResultSet result) throws SQLException {
        LockedCaregiver lc = null;
        lc = new LockedCaregiver(result.getInt(1), result.getString(2), result.getString(3),
                result.getString(4));
        return lc;
    }

    @Override
    protected String getReadAllStatementString() {
        return "SELECT * FROM CAREGIVER";
    }

    @Override
    protected ArrayList<LockedCaregiver> getListFromResultSet(ResultSet result) throws SQLException {
        ArrayList<LockedCaregiver> lCaregivers = new ArrayList<LockedCaregiver>();
        LockedCaregiver lCaregiver = null;
        while (result.next()) {
            lCaregiver = new LockedCaregiver(result.getInt(1), result.getString(2), result.getString(3), result.getString(4));
            lCaregivers.add(lCaregiver);
        }
        return lCaregivers;
    }

    @Override
    protected String getUpdateStatementString(LockedCaregiver lCaregiver) {
        return String.format("UPDATE caregiver SET" +
                        "firstname = '%s'," +
                        "surname = '%s'," +
                        "telephoneNumber = '%s'" +
                        "WHERE caregiverid = %d",
                lCaregiver.getFirstName(),
                lCaregiver.getSurname(),
                lCaregiver.getTelephoneNumber(),
                lCaregiver.getCaregiverID());
    }


    @Override
    protected String getDeleteStatementString(long key) {
        return String.format("Delete FROM caregiver WHERE caregiverid=%d;" +
                             "DELETE FROM LOCKEDCAREGIVER WHERE caregiverid=%d;", key, key);
    }
}
