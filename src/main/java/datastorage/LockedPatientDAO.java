package datastorage;

import model.LockedPatient;
import model.Patient;
import utils.DateConverter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
/**
 * äquivalent to link{@PatientDAO} for locking purpose
 */
public class LockedPatientDAO extends DAOimp<LockedPatient> {

    public LockedPatientDAO(Connection conn){super(conn);}

    @Override
    protected String getCreateStatementString(LockedPatient patient) {
        return String.format("INSERT INTO LOCKEDPATIENTS (pid, firstname, surname, dateOfBirth, carelevel, roomnumber) VALUES ('%s', '%s', '%s', '%s', '%s', '%s');",
                patient.getPid(), patient.getFirstName(), patient.getSurname(), patient.getDateOfBirth(), patient.getCareLevel(), patient.getRoomnumber());
    }

    @Override
    protected String getReadByIDStatementString(long key) {
        return null;
    }

    @Override
    protected LockedPatient getInstanceFromResultSet(ResultSet set) throws SQLException {
        return null;
    }

    @Override
    protected String getReadAllStatementString() {
        return "SELECT * FROM LOCKEDPATIENTS";    }

    @Override
    protected ArrayList<LockedPatient> getListFromResultSet(ResultSet result) throws SQLException {
        ArrayList<LockedPatient> list = new ArrayList<LockedPatient>();
        LockedPatient lp = null;
        while (result.next()) {
            LocalDate date = DateConverter.convertStringToLocalDate(result.getString(4));
            lp = new LockedPatient(result.getInt(1), result.getString(2),
                    result.getString(3), date,
                    result.getString(5), result.getString(6));
            list.add(lp);
        }
        return list;
    }

    @Override
    protected String getUpdateStatementString(LockedPatient lpatient) {
        return null;
    }

    @Override
    protected String getDeleteStatementString(long key) {
        return String.format("Delete FROM patient WHERE pid=%d;" +
                             "DELETE FROM LOCKEDPATIENTS WHERE pid=%d;", key, key);
    }
}
