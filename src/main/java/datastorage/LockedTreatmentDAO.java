package datastorage;

import model.LockedPatient;
import model.LockedTreatment;
import model.Treatment;
import utils.DateConverter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
/**
 * äquivalent to link{@TreatmentDAO} for locking purpose
 */
public class LockedTreatmentDAO extends DAOimp<LockedTreatment>{
    public LockedTreatmentDAO(Connection conn) {
        super(conn);
    }

    @Override
    protected String getCreateStatementString(LockedTreatment lTreatment) {
        return String.format("INSERT INTO LOCKEDTREATMENTS (pid, treatment_date, begin, end, description, remarks, caregiver) VALUES (%d, '%s', '%s', '%s', '%s', '%s', '%s');",
                        lTreatment.getPid(), lTreatment.getDate(),
                        lTreatment.getBegin(), lTreatment.getEnd(), lTreatment.getDescription(),
                        lTreatment.getRemarks(), lTreatment.getCaregiver());
    }

    @Override
    protected String getReadByIDStatementString(long key) {
        return String.format("SELECT * FROM LOCKEDTREATMENTS WHERE tid = %d", key);
    }

    @Override
    protected LockedTreatment getInstanceFromResultSet(ResultSet result) throws SQLException {
        LocalDate date = DateConverter.convertStringToLocalDate(result.getString(3));
        LocalTime begin = DateConverter.convertStringToLocalTime(result.getString(4));
        LocalTime end = DateConverter.convertStringToLocalTime(result.getString(5));
        LockedTreatment m = new LockedTreatment(result.getLong(1), result.getLong(2),
                date, begin, end, result.getString(6), result.getString(7), result.getLong(8));
        return m;
    }

    @Override
    protected String getReadAllStatementString() {
        return "SELECT * FROM LOCKEDTREATMENTS";
    }

    @Override
    protected ArrayList<LockedTreatment> getListFromResultSet(ResultSet result) throws SQLException {
        ArrayList<LockedTreatment> list = new ArrayList<LockedTreatment>();
        LockedTreatment t = null;
        while (result.next()) {
            LocalDate date = DateConverter.convertStringToLocalDate(result.getString(3));
            LocalTime begin = DateConverter.convertStringToLocalTime(result.getString(4));
            LocalTime end = DateConverter.convertStringToLocalTime(result.getString(5));
            t = new LockedTreatment(result.getLong(1), result.getLong(2),
                    date, begin, end, result.getString(6), result.getString(7), result.getLong(8));
            list.add(t);
        }
        return list;
    }

    @Override
    protected String getUpdateStatementString(LockedTreatment lTreatment) {
        return String.format("UPDATE LOCKEDTREATMENTS SET pid = %d, treatment_date ='%s', begin = '%s', end = '%s'," +
                        "description = '%s', remarks = '%s', caregiver = '%s' WHERE tid = %d", lTreatment.getPid(), lTreatment.getDate(),
                lTreatment.getBegin(), lTreatment.getEnd(), lTreatment.getDescription(), lTreatment.getRemarks(),
                lTreatment.getTid(), lTreatment.getCaregiver());
    }

    @Override
    protected String getDeleteStatementString(long key) {
        return String.format("Delete FROM TREATMENT WHERE tid= %d", key);
    }

    public List<LockedTreatment> readTreatmentsByPid(long pid) throws SQLException {
        ArrayList<LockedTreatment> list = new ArrayList<LockedTreatment>();
        Treatment object = null;
        Statement st = conn.createStatement();
        ResultSet result = st.executeQuery(getReadAllTreatmentsOfOnePatientByPid(pid));
        list = getListFromResultSet(result);
        return list;
    }

    private String getReadAllTreatmentsOfOnePatientByPid(long pid){
        return String.format("SELECT * FROM LOCKEDTREATMENTS WHERE pid = %d", pid);
    }

    public void deleteByPid(long key) throws SQLException {
        Statement st = conn.createStatement();
        st.executeUpdate(String.format("Delete FROM LOCKEDTREATMENTS WHERE pid= %d", key));
    }

    public List<LockedTreatment> getReadAllTreatmentsOfOneCaregiverByCaregiverid(long key){
        try {
            ArrayList<LockedTreatment> list = new ArrayList<LockedTreatment>();
            Statement st = conn.createStatement();
            ResultSet result = st.executeQuery(String.format("SELECT * FROM LOCKEDTREATMENTS WHERE caregiver=%d", key));
            list = getListFromResultSet(result);
            return list;
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }
}
