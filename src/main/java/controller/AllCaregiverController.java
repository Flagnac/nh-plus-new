package controller;
import datastorage.CaregiverDAO;
import datastorage.DAOFactory;
import datastorage.LockedCaregiverDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import model.Caregiver;
import model.LockedCaregiver;

import java.sql.SQLException;
import java.util.List;
/**
 * The <code>AllCaregiverController</code> contains the entire logic of the caregiver view. It determines which data is displayed and how to react to events.
 */
public class AllCaregiverController {
    @FXML
    private TableView<Caregiver> tableView;
    @FXML
    private TableColumn<Caregiver, Integer> colID;
    @FXML
    private TableColumn<Caregiver, String> colFirstName;
    @FXML
    private TableColumn<Caregiver, String> colSurname;
    @FXML
    private TableColumn<Caregiver, String> colTelephone;

    @FXML
    Button btnAdd;
    @FXML
    Button btnLock;
    @FXML
    TextField txtFirstname;
    @FXML
    TextField txtSurname;
    @FXML
    TextField txtTelephoneNumber;


    private ObservableList<Caregiver> tableviewContent = FXCollections.observableArrayList();
    private CaregiverDAO dao;

    /**
     * Initializes the corresponding fields. Is called as soon as the corresponding FXML file is to be displayed.
     */
    public void initialize() {
        readAllAndShowInTableView();

        this.colID.setCellValueFactory(new PropertyValueFactory<Caregiver, Integer>("CaregiverID"));

        this.colFirstName.setCellValueFactory(new PropertyValueFactory<Caregiver, String>("FirstName"));
        this.colFirstName.setCellFactory(TextFieldTableCell.forTableColumn());

        this.colSurname.setCellValueFactory(new PropertyValueFactory<Caregiver, String>("Surname"));
        this.colSurname.setCellFactory(TextFieldTableCell.forTableColumn());

        this.colTelephone.setCellValueFactory(new PropertyValueFactory<Caregiver, String>("TelephoneNumber"));
        this.colTelephone.setCellFactory(TextFieldTableCell.forTableColumn());

        this.tableView.setItems(this.tableviewContent);
    }

    @FXML
    public void handleOnEditFirstname(TableColumn.CellEditEvent<Caregiver, String> event){
        event.getRowValue().setFirstName(event.getNewValue());
        doUpdate(event);
    }

    @FXML
    public void handleOnEditSurname(TableColumn.CellEditEvent<Caregiver, String> event){
        event.getRowValue().setSurname(event.getNewValue());
        doUpdate(event);
    }

    @FXML
    public void handleOnEditTelephoneNumber(TableColumn.CellEditEvent<Caregiver, String> event){
        event.getRowValue().setTelephoneNumber(event.getNewValue());
        doUpdate(event);
    }

    private void doUpdate(TableColumn.CellEditEvent<Caregiver, String> t) {
        try {
            dao.update(t.getRowValue());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /**
     * Calls readAll in {@link CaregiverDAO} and shows Caregivers in the table
     */
    private void readAllAndShowInTableView() {
        this.tableviewContent.clear();
        this.dao = DAOFactory.getDAOFactory().createCaregiverDAO();
        List<Caregiver> allCaregivers;
        try {
            allCaregivers = dao.readAll();
            for (Caregiver c : allCaregivers) {
                this.tableviewContent.add(c);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /**
     * Handles a add-click-event. Creates a patient and calls the create method in the {@link CaregiverDAO}
     */
    @FXML
    public void handleAdd() {
        String firstname = this.txtFirstname.getText();
        String surname = this.txtSurname.getText();
        String telephoneNumber = this.txtTelephoneNumber.getText();

        try {
            Caregiver c = new Caregiver(firstname, surname, telephoneNumber);
            dao.create(c);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        readAllAndShowInTableView();
        clearTextfields();
    }

    /**
     * Locks the Caregiver on click
     */
    @FXML
    public void handleLock() {
        Caregiver cToLock = this.tableView.getSelectionModel().getSelectedItem();
        LockedCaregiverDAO lcDAO = DAOFactory.getDAOFactory().creatLockedCaregiverDAO();
        LockedCaregiver selectedItem = new LockedCaregiver(cToLock.getCaregiverID(), cToLock.getFirstName(), cToLock.getSurname(), cToLock.getTelephoneNumber());
        try{
            lcDAO.lock(selectedItem, selectedItem.getCaregiverID());
        } catch(SQLException e){
            e.printStackTrace();
        }
    }

    /**
     * Removes content from all Fields
     */
    private void clearTextfields() {
        this.txtFirstname.clear();
        this.txtSurname.clear();
        this.txtTelephoneNumber.clear();

    }
}
