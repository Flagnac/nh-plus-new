package controller;

import datastorage.DAOFactory;
import datastorage.UserDAO;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import model.User;
import java.io.IOException;
import java.sql.SQLException;

public class LoginController {
    @FXML
    private TextField txtuser;
    @FXML
    private PasswordField txtpasswort;
    @FXML
    private Button anmeldebut;

    /**
     * executes the button from the <code>Login.fxml</code> when it is pressed
     * it is checked if the password matches the username
     * if it is true, the Main Window opens, if it is not true, the password is wrong
     * if the user does not exist in the database, the user is incorrect.
     * @throws IOException is thrown when it runs into an error.
     */
    @FXML
    public void handlelogin() throws IOException {
        UserDAO dao = DAOFactory.getDAOFactory().createUserDAO();
        String Name = txtuser.getText();
        String Passwort = txtpasswort.getText();
        try {
            User user = dao.getUserlogindata(Name);
            if (user != null) {
                String usernamedb = user.getName();
                String userpassworddb = user.getPasswort();
                if (Passwort.equals(userpassworddb)) {
                    Mainwindow();
                } else {
                    Alert alertpasswort = new Alert(Alert.AlertType.WARNING);
                    alertpasswort.setTitle("Warning Dialog");
                    alertpasswort.setHeaderText("Ungültiges Passwort!");
                    alertpasswort.setContentText("Ihr Passwort ist Falsch! Bitte versuchen Sie es erneut.");
                    alertpasswort.show();
                }
            } else {
                Alert alertbenutzer = new Alert(Alert.AlertType.WARNING);
                alertbenutzer.setTitle("Warning Dialog");
                alertbenutzer.setHeaderText("Ungültiger Benutzername!");
                alertbenutzer.setContentText("Ihr Benutzername ist Falsch! Bitte versuchen Sie es erneut.");
                alertbenutzer.show();
            }
        } catch (SQLException throwables) {
            Alert alerterror = new Alert(Alert.AlertType.ERROR);
            alerterror.setTitle("Error Dialog");
            alerterror.setHeaderText("Fehler!");
            alerterror.setContentText("Keine Verbindung zu der Datenbank!");
            alerterror.show();
        }
    }

    /**
     * if the user entered in <code>Login.fxml</code> matches the password, the Main Window opens
     * the MainWindowView.fxml is thrown onto a canvas
     * this screen is placed on a stage and the curtain is lifted.
     * @throws IOException
     */
    private void Mainwindow() throws IOException {
        Stage primaryStage = (Stage) txtuser.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("/MainWindowView.fxml"));
        BorderPane pane = loader.load();
        Scene scene = new Scene(pane);
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();
    }
}